﻿using ConsumindoApiViaCep.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiViaCep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite Seu CEP: ");
            string Cep = Console.ReadLine();
                string url = "https://viacep.com.br/ws/" + Cep +  "/json/";

           
            //Consumindo API
            ViaCep viaCep = BuscarCep(url);
           
            
            Console.WriteLine(viaCep.Cep);
            Console.WriteLine(viaCep.Logradouro);
            Console.WriteLine(viaCep.Complemento);
            Console.WriteLine(viaCep.Bairro);
            Console.WriteLine(viaCep.Localidade);
            Console.WriteLine(viaCep.Uf);
            Console.WriteLine(viaCep.Unidade);
            Console.WriteLine(viaCep.Ibge);
            Console.WriteLine(viaCep.Gia);
                Console.WriteLine("\n\n");
            //Console.WriteLine("Conhecimentos");


            //foreach (string conhecimentos in Usuario.Conhecimentos)
            //{
            //    Console.WriteLine(conhecimentos);
            //}
            ////Console.WriteLine(Computador.Softwares.ToString());
            //Console.WriteLine("\n\n");
            //Console.WriteLine("Formações");
            //foreach (Qualificacao Qualificacao in Usuario.Qualificacoes)
            //{
            //    Console.WriteLine(Qualificacao.Nome);
            //    Console.WriteLine(Qualificacao.Instituicao);
            //    Console.WriteLine(Qualificacao.Ano);
            //}

            Console.WriteLine("Digite Seu CEP: ", viaCep.Cep);
            
            Console.ReadLine();
            }

            public static ViaCep BuscarCep(string url)
            {
                //Instanciando webclient para chamadas http
                WebClient wc = new WebClient();
                string content = wc.DownloadString(url);

                Console.WriteLine(content);
                Console.WriteLine("\n\n");

                ViaCep viaCep = JsonConvert.DeserializeObject<ViaCep>(content);

                return viaCep;
            }
        }
    }

