﻿using ConsumindoApiUsuario.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiUsuario
{
    class Program
    {
        static void Main(string[] args)
        {
            
                string url = "http://senacao.tk/objetos/usuario";
              

                //Consumindo API
                Usuario Usuario = BuscarUsuario(url);
                Console.WriteLine("Usuario");
                Console.WriteLine(String.Format("Nome: {0} - Email: {1} - Telefone: {2} - Endereço: {3}", Usuario.Nome , Usuario.Email, Usuario.Telefone, Usuario.Endereco));
                Console.WriteLine("\n\n");
                Console.WriteLine("Conhecimentos");
                

            foreach (string conhecimentos in Usuario.Conhecimentos)
                {
                    Console.WriteLine(conhecimentos);
                }
            //Console.WriteLine(Computador.Softwares.ToString());
            Console.WriteLine("\n\n");
            Console.WriteLine("Formações");
            foreach (Qualificacao Qualificacao in Usuario.Qualificacoes)
            {
                Console.WriteLine(Qualificacao.Nome);
                Console.WriteLine(Qualificacao.Instituicao);
                Console.WriteLine(Qualificacao.Ano);
            }

            Console.ReadLine();
           }

            public static Usuario BuscarUsuario(string url)
            {
                //Instanciando webclient para chamadas http
                WebClient wc = new WebClient();
                string content = wc.DownloadString(url);

                Console.WriteLine(content);
                Console.WriteLine("\n\n");

               Usuario usuario = JsonConvert.DeserializeObject<Usuario>(content);

                return usuario;
            }
        }
    }
