﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConvertendoJasonParaClasse.Models
{
    class Carro
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Quilometragem { get; set; }

        public List<string> Opcionais { get; set; }
        public Vendedor Vendedor { get; set; }
    }

    class Vendedor
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string Celular { get; set; }
        public string Cidade { get; set; }
    }
      
}
