﻿using ConvertendoJasonParaClasse.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiVeiculo
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/veiculo_array_objeto";

            //Consumindo API
            Carro Carro = BuscarCarro(url);
            Console.WriteLine("Carro");
            Console.WriteLine(String.Format("Marca: {0} - Modelo: {1} - Ano: {2} - Quilometragem: {3}", Carro.Marca, Carro.Modelo, Carro.Ano, Carro.Quilometragem));
            Console.WriteLine("\n\n");
            Console.WriteLine("Opcionais");
            Console.WriteLine(String.Format("Nome: {0} - Idade: {1} - Celular: {2} - Cidade: {3}", Carro.Vendedor.Nome, Carro.Vendedor.Idade, Carro.Vendedor.Celular, Carro.Vendedor.Cidade));
            Console.WriteLine("\n\n");
            Console.WriteLine("Vendedor");

            foreach (string opcionais in Carro.Opcionais)
            {
               Console.WriteLine(opcionais);
            }
            //Console.WriteLine(Computador.Softwares.ToString());


            Console.ReadLine();
        }

        public static Carro BuscarCarro(string url)
        {
            //Instanciando webclient para chamadas http
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

           Carro Carro = JsonConvert.DeserializeObject<Carro>(content);

            return Carro;
        }
    }
    
}
