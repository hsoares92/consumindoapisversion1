﻿using ApiUsuarioAleatorio.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ApiUsuarioAleatorio
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o numero de usuarios que Procura?");
            string Numero = Console.ReadLine();
            string url = "https://randomuser.me/api/";
            RandomUsers random = BuscarRandom(url);

            Console.ReadLine();
        }

        public static RandomUsers BuscarRandom(string url)
        {
            //Instanciando webclient para chamadas http
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            RandomUsers random = JsonConvert.DeserializeObject<RandomUsers>(content);

            return random;
        }
    }
}
